package twenty_nineteen

// PacketHeader 2019
type PacketHeader struct {
	PacketFormat     uint16
	GameMajorVersion uint8
	GameMinorVersion uint8
	PacketVersion    uint8
	PacketId         uint8
	SessionUID       uint64
	SessionTime      float32
	FrameIdentifier  uint32
	PlayerCarIndex   uint8
}

// PacketMotionData 2019
// The motion packet gives physics data for all the cars being driven. There is additional data for the car being
// driven with the goal of being able to drive a motion platform setup.
// N.B. For the normalised vectors below, to convert to float values divide by 32767.0f – 16-bit signed values are
// used to pack the data and on the assumption that direction values are always between -1.0f and 1.0f.
// Frequency: Rate as specified in menus
// Size: MotionPacketLength bytes
// Version: 1
type PacketMotionData struct {
	Header                 PacketHeader
	CarMotionData          [20]CarMotionData
	SuspensionPosition     [4]float32
	SuspensionVelocity     [4]float32
	SuspensionAcceleration [4]float32
	WheelSpeed             [4]float32
	WheelSlip              [4]float32
	LocalVelocityX         float32
	LocalVelocityY         float32
	LocalVelocityZ         float32
	AngularVelocityX       float32
	AngularVelocityY       float32
	AngularVelocityZ       float32
	AngularAccelerationX   float32
	AngularAccelerationY   float32
	AngularAccelerationZ   float32
	FrontWheelsAngle       float32
}

// CarMotionData 2019
type CarMotionData struct {
	WorldPositionX     float32
	WorldPositionY     float32
	WorldPositionZ     float32
	WorldVelocityX     float32
	WorldVelocityY     float32
	WorldVelocityZ     float32
	WorldForwardDirX   int16
	WorldForwardDirY   int16
	WorldForwardDirZ   int16
	WorldRightDirX     int16
	WorldRightDirY     int16
	WorldRightDirZ     int16
	GForceLateral      float32
	GForceLongitudinal float32
	GForceVertical     float32
	Yaw                float32
	Pitch              float32
	Roll               float32
}

// PacketSessionData 2019
// The session packet includes details about the current session in progress.
// Frequency: 2 per second
// Size: SessionPacketLength bytes
// Version: 1
type PacketSessionData struct {
	Header              PacketHeader
	Weather             uint8
	TrackTemperature    int8
	AirTemperature      int8
	TotalLaps           uint8
	TrackLength         uint16
	SessionType         uint8
	TrackId             int8
	Formula             uint8
	SessionTimeLeft     uint16
	SessionDuration     uint16
	PitSpeedLimit       uint8
	GamePaused          uint8
	IsSpectating        uint8
	SpectatorCarIndex   uint8
	SliProNativeSupport uint8
	NumMarshalZones     uint8
	MarshalZones        [21]MarshalZone
	SafetyCarStatus     uint8
	NetworkGame         uint8
}

// MarshalZone 2019
type MarshalZone struct {
	ZoneStart float32
	ZoneFlag  int8
}
