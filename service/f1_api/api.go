package f1_api

import (
	"bytes"
	"encoding/binary"
	"io"
	"net"

	"f1-race-control/server-app/twenty_twenty"
)

const (
	PacketIDMotion              uint8 = 0
	PacketIDSession             uint8 = 1
	PacketIDLapData             uint8 = 2
	PacketIDEvent               uint8 = 3
	PacketIDParticipants        uint8 = 4
	PacketIDCarSetups           uint8 = 5
	PacketIDCarTelemetry        uint8 = 6
	PacketIDCarStatus           uint8 = 7
	PacketIDFinalClassification uint8 = 8
	PacketIDLobbyInformation    uint8 = 9
)

var decoderMap = map[uint16]map[uint8]Decoder{
	2020: {
		PacketIDCarSetups:           twenty_twenty.CarSetupsDecoder,
		PacketIDCarStatus:           twenty_twenty.CarStatusDecoder,
		PacketIDEvent:               twenty_twenty.EventDecoder,
		PacketIDFinalClassification: twenty_twenty.FinalClassificationDecoder,
		PacketIDLapData:             twenty_twenty.LapDecoder,
		PacketIDLobbyInformation:    twenty_twenty.LobbyInfoDecoder,
		PacketIDMotion:              twenty_twenty.MotionDecoder,
		PacketIDParticipants:        twenty_twenty.ParticipantsDecoder,
		PacketIDCarTelemetry:        twenty_twenty.CarTelemetryDecoder,
		PacketIDSession:             twenty_twenty.SessionDecoder,
	},
}

type Decoder func(raw io.Reader) (interface{}, error)

type Api struct {
	conn *net.UDPConn
	out  chan interface{}
}

func NewApi(conn *net.UDPConn) *Api {
	return &Api{
		conn: conn,
		out:  make(chan interface{}, 10),
	}
}

func (api *Api) Listen() {
	var err error
	buf := make([]byte, 1536)
	input := make(chan []byte, 5)
	go api.decode(input)
	// listen for telemetry data
	for {
		_, _, err = api.conn.ReadFromUDP(buf)
		if err != nil {
			println("udp read err: " + err.Error())
			continue
		}
		raw := make([]byte, len(buf))
		copy(raw, buf)
		input <- raw
	}
}

func (api *Api) Output() <-chan interface{} {
	return api.out
}

func (api *Api) decode(in <-chan []byte) {
	var raw []byte
	var ok bool
	var pktReader io.Reader
	var version map[uint8]Decoder
	var decoder Decoder
	var pkt interface{}
	var err error
	for {
		raw, ok = <-in
		if !ok {
			continue
		}
		pktReader = bytes.NewReader(raw)
		if version, ok = decoderMap[binary.LittleEndian.Uint16(raw[:2])]; ok {
			if decoder, ok = version[raw[5]]; ok {
				pkt, err = decoder(pktReader)
				if err != nil {
					println("packet decode err: " + err.Error())
					continue
				}
				api.out <- pkt
			}
		}
	}
}
