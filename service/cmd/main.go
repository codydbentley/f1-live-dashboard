package main

import (
	"fmt"
	"log"
	"net"
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"

	"f1-race-control/server-app/f1_api"
)

func main() {
	addr, err := net.ResolveUDPAddr("udp", "127.0.0.1:20777")
	if err != nil {
		panic("udp address error: " + err.Error())
	}

	conn, err := net.ListenUDP("udp", addr)
	if err != nil {
		panic("udp listen error: " + err.Error())
	}
	defer conn.Close()

	f1api := f1_api.NewApi(conn)
	output := f1api.Output()
	clients := make(map[string]*websocket.Conn)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		var err error
		u := websocket.Upgrader{}
		u.CheckOrigin = func(_ *http.Request) bool {
			return true
		}
		ws, err := u.Upgrade(w, r, nil)
		if err != nil {
			println("unable to create app socket: " + err.Error())
			return
		}
		uid, err := uuid.NewUUID()
		if err != nil {
			ws.Close()
			return
		}
		id := uid.String()
		clients[id] = ws
	})
	go func() {
		var err error
		for {
			pkt, ok := <-output
			if !ok {
				println("pkt receive error")
				continue
			}
			for id, client := range clients {
				err = client.WriteJSON(pkt)
				if err != nil {
					client.Close()
					delete(clients, id)
					println("socket write error: " + err.Error())
				}
			}
		}
	}()
	go f1api.Listen()
	fmt.Println("F1 Live Dashboard Online")
	log.Fatal(http.ListenAndServe("127.0.0.1:20777", nil))
}
