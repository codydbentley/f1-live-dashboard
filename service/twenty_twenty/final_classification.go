package twenty_twenty

const FinalClassificationPacketLength = 839

// PacketFinalClassificationData
// This packet details the final classification at the end of the race, and the data will match with the post race
// results screen. This is especially useful for multiplayer games where it is not always possible to send lap times
// on the final frame because of network delay.
// Frequency: Once at the end of a race
// Size: 839 bytes
// Version: 1
// - Header
// - NumCars Number of cars in the final classification
// - ClassificationData
type PacketFinalClassificationData struct {
	Header             PacketHeader
	NumCars            uint8
	ClassificationData [22]FinalClassificationData
}

// FinalClassificationData
// - Position         Finishing position
// - NumLaps          Number of laps completed
// - GridPosition     Grid position of the car
// - Points           Number of points scored
// - NumPitStops      Number of pit stops made
// - ResultStatus     Result status
// - BestLapTime      Best lap time of the session in seconds
// - TotalRaceTime    Total race time in seconds without penalties
// - PenaltiesTime    Total penalties accumulated in seconds
// - NumPenalties     Number of penalties applied to this driver
// - NumTyreStints    Number of tyres stints up to maximum
// - TyreStintsActual Actual tyres used by this driver
// - TyreStintsVisual Visual tyres used by this driver
type FinalClassificationData struct {
	Position         uint8
	NumLaps          uint8
	GridPosition     uint8
	Points           uint8
	NumPitStops      uint8
	ResultStatus     uint8
	BestLapTime      float32
	TotalRaceTime    float64
	PenaltiesTime    uint8
	NumPenalties     uint8
	NumTyreStints    uint8
	TyreStintsActual [8]uint8
	TyreStintsVisual [8]uint8
}
