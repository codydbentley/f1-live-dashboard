package twenty_twenty

import (
	"bytes"
	"encoding/binary"
	"io"
)

func CarSetupsDecoder(raw io.Reader) (interface{}, error) {
	pkt := PacketCarSetupData{}
	err := binary.Read(raw, binary.LittleEndian, &pkt)
	return pkt, err
}

func CarStatusDecoder(raw io.Reader) (interface{}, error) {
	pkt := PacketCarStatusData{}
	err := binary.Read(raw, binary.LittleEndian, &pkt)
	return pkt, err
}

func EventDecoder(raw io.Reader) (interface{}, error) {
	eventData := PacketEventData{}
	err := binary.Read(raw, binary.LittleEndian, &eventData)
	evnt := PacketEventDataParsed{
		Header:          eventData.Header,
		EventStringCode: string(eventData.EventStringCode[:]),
	}
	var details interface{}
	switch evnt.EventStringCode {
	case EventFastestLap:
		dt := FastestLap{}
		pktReader := bytes.NewReader(eventData.EventDetails[:])
		err = binary.Read(pktReader, binary.LittleEndian, &dt)
		details = dt
		break
	case EventRetirement:
		dt := Retirement{}
		pktReader := bytes.NewReader(eventData.EventDetails[:])
		err = binary.Read(pktReader, binary.LittleEndian, &dt)
		details = dt
		break
	case EventTeamMatePit:
		dt := TeamMateInPits{}
		pktReader := bytes.NewReader(eventData.EventDetails[:])
		err = binary.Read(pktReader, binary.LittleEndian, &dt)
		details = dt
		break
	case EventRaceWinner:
		dt := RaceWinner{}
		pktReader := bytes.NewReader(eventData.EventDetails[:])
		err = binary.Read(pktReader, binary.LittleEndian, &dt)
		details = dt
		break
	case EventPenaltyIssued:
		dt := Penalty{}
		pktReader := bytes.NewReader(eventData.EventDetails[:])
		err = binary.Read(pktReader, binary.LittleEndian, &dt)
		details = dt
		break
	case EventSpeedTrap:
		dt := SpeedTrap{}
		pktReader := bytes.NewReader(eventData.EventDetails[:])
		err = binary.Read(pktReader, binary.LittleEndian, &dt)
		details = dt
		break
	default:
		details = struct{}{}
		err = nil
	}
	evnt.EventDetails = details
	return evnt, err
}

func FinalClassificationDecoder(raw io.Reader) (interface{}, error) {
	pkt := PacketFinalClassificationData{}
	err := binary.Read(raw, binary.LittleEndian, &pkt)
	return pkt, err
}

func LapDecoder(raw io.Reader) (interface{}, error) {
	pkt := PacketLapData{}
	err := binary.Read(raw, binary.LittleEndian, &pkt)
	return pkt, err
}

func LobbyInfoDecoder(raw io.Reader) (interface{}, error) {
	pkt := PacketLobbyInfoData{}
	err := binary.Read(raw, binary.LittleEndian, &pkt)
	if err != nil {
		return pkt, err
	}
	parsed := PacketLobbyInfoDataParsed{
		Header:     pkt.Header,
		NumPlayers: pkt.NumPlayers,
	}
	for i, lobby := range pkt.LobbyPlayers {
		parsed.LobbyPlayers[i].AiControlled = lobby.AiControlled
		parsed.LobbyPlayers[i].TeamId = lobby.TeamId
		parsed.LobbyPlayers[i].Nationality = lobby.Nationality
		parsed.LobbyPlayers[i].Name = string(bytes.Trim(lobby.Name[:], "\x00"))
		parsed.LobbyPlayers[i].ReadyStatus = lobby.ReadyStatus
	}
	return parsed, err
}

func MotionDecoder(raw io.Reader) (interface{}, error) {
	pkt := PacketMotionData{}
	err := binary.Read(raw, binary.LittleEndian, &pkt)
	return pkt, err
}

func ParticipantsDecoder(raw io.Reader) (interface{}, error) {
	pkt := PacketParticipantsData{}
	err := binary.Read(raw, binary.LittleEndian, &pkt)
	if err != nil {
		return pkt, err
	}
	parsed := PacketParticipantsDataParsed{
		Header:        pkt.Header,
		NumActiveCars: pkt.NumActiveCars,
	}
	for i, part := range pkt.Participants {
		parsed.Participants[i].AiControlled = part.AiControlled
		parsed.Participants[i].DriverId = part.DriverId
		parsed.Participants[i].TeamId = part.TeamId
		parsed.Participants[i].RaceNumber = part.RaceNumber
		parsed.Participants[i].Nationality = part.Nationality
		parsed.Participants[i].Name = string(bytes.Trim(part.Name[:], "\x00"))
		parsed.Participants[i].YourTelemetry = part.YourTelemetry
	}
	return parsed, err
}

func CarTelemetryDecoder(raw io.Reader) (interface{}, error) {
	pkt := PacketCarTelemetryData{}
	err := binary.Read(raw, binary.LittleEndian, &pkt)
	return pkt, err
}

func SessionDecoder(raw io.Reader) (interface{}, error) {
	pkt := PacketSessionData{}
	err := binary.Read(raw, binary.LittleEndian, &pkt)
	return pkt, err
}
