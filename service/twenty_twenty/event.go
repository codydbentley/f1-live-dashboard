package twenty_twenty

const (
	EventSessionStarted = "SSTA"
	EventSessionEnded   = "SEND"
	EventFastestLap     = "FLTP"
	EventRetirement     = "RTMT"
	EventDrsEnabled     = "DRSE"
	EventDrsDisabled    = "DRSD"
	EventTeamMatePit    = "TMPT"
	EventChequeredFlag  = "CHQF"
	EventRaceWinner     = "RCWN"
	EventPenaltyIssued  = "PENA"
	EventSpeedTrap      = "SPTP"
)

// PacketEventData
// This packet gives details of events that happen during the course of a session.
// Frequency: When the event occurs
// Size: 35 bytes
// Version: 1
// - Header
// - EventStringCode Event string code
// - EventDetails    Event details - should be interpreted differently for each type
type PacketEventData struct {
	Header          PacketHeader
	EventStringCode [4]byte
	EventDetails    [7]byte
}

// PacketEventDataParsed
type PacketEventDataParsed struct {
	Header          PacketHeader
	EventStringCode string
	EventDetails    interface{}
}

// FastestLap
// - VehicleIdx Vehicle index of car achieving fastest lap
// - LapTime    Lap time is in seconds
type FastestLap struct {
	VehicleIdx uint8
	LapTime    float32
}

// Retirement
//  - VehicleIdx Vehicle index of car retiring
type Retirement struct {
	VehicleIdx uint8
}

// TeamMateInPits
//  - VehicleIdx Vehicle index of team mate
type TeamMateInPits struct {
	VehicleIdx uint8
}

// RaceWinner
//  - VehicleIdx Vehicle index of the race winner
type RaceWinner struct {
	VehicleIdx uint8
}

// Penalty
// - PenaltyType      Penalty type – see Appendices
// - InfringementType Infringement type – see Appendices
// - VehicleIdx       Vehicle index of the car the penalty is applied to
// - OtherVehicleIdx  Vehicle index of the other car involved
// - Time             Time gained, or time spent doing action in seconds
// - LapNum           Lap the penalty occurred on
// - PlacesGained     Number of places gained by this
type Penalty struct {
	PenaltyType      uint8
	InfringementType uint8
	VehicleIdx       uint8
	OtherVehicleIdx  uint8
	Time             uint8
	LapNum           uint8
	PlacesGained     uint8
}

// SpeedTrap
// - VehicleIdx Vehicle index of the vehicle triggering speed trap
// - Speed      Top speed achieved in kilometres per hour
type SpeedTrap struct {
	VehicleIdx uint8
	Speed      float32
}
