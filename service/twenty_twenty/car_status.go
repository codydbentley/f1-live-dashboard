package twenty_twenty

const CarStatusPacketLength = 1344

// PacketCarStatusData
// This packet details car statuses for all the cars in the race. It includes values such as the damage readings on the car.
// Frequency: Rate as specified in menus
// Size: 1344 bytes
// Version: 1
// - Header
// - CarStatusData
type PacketCarStatusData struct {
	Header        PacketHeader
	CarStatusData [22]CarStatusData
}

// CarStatusData
// - TractionControl         0 (off) - 2 (high)
// - AntiLockBrakes          0 (off) - 1 (on)
// - FuelMix                 Fuel mix - 0 = lean, 1 = standard, 2 = rich, 3 = max
// - FrontBrakeBias          Front brake bias (percentage)
// - PitLimiterStatus        Pit limiter status - 0 = off, 1 = on
// - FuelInTank              Current fuel mass
// - FuelCapacity            Fuel capacity
// - FuelRemainingLaps       Fuel remaining in terms of laps (value on MFD)
// - MaxRPM                  Cars max RPM, point of rev limiter
// - IdleRPM                 Cars idle RPM
// - MaxGears                Maximum number of gears
// - DrsAllowed              0 = not allowed, 1 = allowed, -1 = unknown
// - DrsActivationDistance   0 = DRS not available, non-zero - DRS will be available in [X] metres
// - TyresWear               Tyre wear percentage
// - ActualTyreCompound      F1 Modern - 16 = C5, 17 = C4, 18 = C3, 19 = C2, 20 = C1 7 = inter, 8 = wet F1 Classic - 9 = dry, 10 = wet F2 – 11 = super soft, 12 = soft, 13 = medium, 14 = hard 15 = wet
// - TyreVisualCompound      F1 visual (can be different from actual compound) 16 = soft, 17 = medium, 18 = hard, 7 = inter, 8 = wet F1 Classic – same as above F2 – 19 = super soft, 20 = soft, 21 = medium, 22 = hard 15 = wet
// - TyresAgeLaps            Age in laps of the current set of tyres
// - TyresDamage             Tyre damage (percentage)
// - FrontLeftWingDamage     Front left wing damage (percentage)
// - FrontRightWingDamage    Front right wing damage (percentage)
// - RearWingDamage          Rear wing damage (percentage)
// - DrsFault                Indicator for DRS fault, 0 = OK, 1 = fault
// - EngineDamage            Engine damage (percentage)
// - GearBoxDamage           Gear box damage (percentage)
// - VehicleFiaFlags         -1 = invalid/unknown, 0 = none, 1 = green 2 = blue, 3 = yellow, 4 = red
// - ErsStoreEnergy          ERS energy store in Joules
// - ErsDeployMode           ERS deployment mode, 0 = none, 1 = medium 2 = overtake, 3 = hotlap
// - ErsHarvestedThisLapMGUK ERS energy harvested this lap by MGU-K
// - ErsHarvestedThisLapMGUH ERS energy harvested this lap by MGU-H
// - ErsDeployedThisLap      ERS energy deployed this lap
type CarStatusData struct {
	TractionControl         uint8
	AntiLockBrakes          uint8
	FuelMix                 uint8
	FrontBrakeBias          uint8
	PitLimiterStatus        uint8
	FuelInTank              float32
	FuelCapacity            float32
	FuelRemainingLaps       float32
	MaxRPM                  uint16
	IdleRPM                 uint16
	MaxGears                uint8
	DrsAllowed              uint8
	DrsActivationDistance   uint16
	TyresWear               [4]uint8
	ActualTyreCompound      uint8
	TyreVisualCompound      uint8
	TyresAgeLaps            uint8
	TyresDamage             [4]uint8
	FrontLeftWingDamage     uint8
	FrontRightWingDamage    uint8
	RearWingDamage          uint8
	DrsFault                uint8
	EngineDamage            uint8
	GearBoxDamage           uint8
	VehicleFiaFlags         int8
	ErsStoreEnergy          float32
	ErsDeployMode           uint8
	ErsHarvestedThisLapMGUK float32
	ErsHarvestedThisLapMGUH float32
	ErsDeployedThisLap      float32
}
