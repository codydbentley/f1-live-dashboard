package twenty_twenty

const CarTelemetryPacketLength = 1307

// PacketCarTelemetryData
// This packet details telemetry for all the cars in the race. It details various values that would be recorded on
// the car such as speed, throttle application, DRS etc.
// Frequency: Rate as specified in menus
// Size: 1307 bytes
// Version: 1
// - Header
// - CarTelemetryData
// - ButtonStatus                 Bit flags specifying which buttons are being pressed currently - see appendices
// - MfdPanelIndex                Index of MFD panel open
// - MfdPanelIndexSecondaryPlayer See above
// - SuggestedGear                Suggested gear for the player (1-8) 0 if no gear suggested
type PacketCarTelemetryData struct {
	Header                       PacketHeader
	CarTelemetryData             [22]CarTelemetryData
	ButtonStatus                 uint32
	MfdPanelIndex                uint8
	MfdPanelIndexSecondaryPlayer uint8
	SuggestedGear                int8
}

// CarTelemetryData
// - Speed                   Speed of car in kilometres per hour
// - Throttle                Amount of throttle applied (0.0 to 1.0)
// - Steer                   Steering (-1.0 (full lock left) to 1.0 (full lock right))
// - Brake                   Amount of brake applied (0.0 to 1.0)
// - Clutch                  Amount of clutch applied (0 to 100)
// - Gear                    Gear selected (1-8, N=0, R=-1)
// - EngineRPM               Engine RPM
// - Drs                     0 = off, 1 = on
// - RevLightsPercent        Rev lights indicator (percentage)
// - BrakesTemperature       Brakes temperature (celsius)
// - TyresSurfaceTemperature Tyres surface temperature (celsius)
// - TyresInnerTemperature   Tyres inner temperature (celsius)
// - EngineTemperature       Engine temperature (celsius)
// - TyresPressure           Tyres pressure (PSI)
// - SurfaceType             Driving surface, see appendices
type CarTelemetryData struct {
	Speed                   uint16
	Throttle                float32
	Steer                   float32
	Brake                   float32
	Clutch                  uint8
	Gear                    int8
	EngineRPM               uint16
	Drs                     uint8
	RevLightsPercent        uint8
	BrakesTemperature       [4]uint16
	TyresSurfaceTemperature [4]uint8
	TyresInnerTemperature   [4]uint8
	EngineTemperature       uint16
	TyresPressure           [4]float32
	SurfaceType             [4]uint8
}
