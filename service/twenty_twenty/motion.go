package twenty_twenty

const MotionPacketLength = 1464

// PacketMotionData
// The motion packet gives physics data for all the cars being driven. There is additional data for the car being
// driven with the goal of being able to drive a motion platform setup.
// N.B. For the normalised vectors below, to convert to float values divide by 32767.0f – 16-bit signed values are
// used to pack the data and on the assumption that direction values are always between -1.0f and 1.0f.
// Frequency: Rate as specified in menus
// Size: MotionPacketLength bytes
// Version: 1
type PacketMotionData struct {
	Header                 PacketHeader
	CarMotionData          [22]CarMotionData
	SuspensionPosition     [4]float32
	SuspensionVelocity     [4]float32
	SuspensionAcceleration [4]float32
	WheelSpeed             [4]float32
	WheelSlip              [4]float32
	LocalVelocityX         float32
	LocalVelocityY         float32
	LocalVelocityZ         float32
	AngularVelocityX       float32
	AngularVelocityY       float32
	AngularVelocityZ       float32
	AngularAccelerationX   float32
	AngularAccelerationY   float32
	AngularAccelerationZ   float32
	FrontWheelsAngle       float32
}

// CarMotionData
type CarMotionData struct {
	WorldPositionX     float32
	WorldPositionY     float32
	WorldPositionZ     float32
	WorldVelocityX     float32
	WorldVelocityY     float32
	WorldVelocityZ     float32
	WorldForwardDirX   int16
	WorldForwardDirY   int16
	WorldForwardDirZ   int16
	WorldRightDirX     int16
	WorldRightDirY     int16
	WorldRightDirZ     int16
	GForceLateral      float32
	GForceLongitudinal float32
	GForceVertical     float32
	Yaw                float32
	Pitch              float32
	Roll               float32
}
