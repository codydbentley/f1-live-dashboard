package twenty_twenty

const ParticipantsPacketLength = 1213

// PacketParticipantsData
// This is a list of participants in the race. If the vehicle is controlled by AI, then the name will be the driver
// name. If this is a multiplayer game, the names will be the Steam Id on PC, or the LAN name if appropriate.
// N.B. on Xbox One, the names will always be the driver name, on PS4 the name will be the LAN name if playing a LAN
// game, otherwise it will be the driver name.
// The array should be indexed by vehicle index.
// Frequency: Every 5 seconds
// Size: 1213 bytes
// Version: 1
// - Header
// - NumActiveCars Number of active cars in the data – should match number of cars on HUD
// - Participants
type PacketParticipantsData struct {
	Header        PacketHeader
	NumActiveCars uint8
	Participants  [22]ParticipantData
}

// ParticipantData
// - AiControlled Whether the vehicle is AI (1) or Human (0) controlled
// - DriverId     Driver id - see appendix
// - TeamId       Team id - see appendix
// - RaceNumber   Race number of the car
// - Nationality  Nationality of the driver
// - Name         Name of participant in UTF-8 format – null terminated. Will be truncated with … (U+2026) if too long
// - YourTelemetry The player's UDP setting, 0 = restricted, 1 = public
type ParticipantData struct {
	AiControlled  uint8
	DriverId      uint8
	TeamId        uint8
	RaceNumber    uint8
	Nationality   uint8
	Name          [48]byte
	YourTelemetry uint8
}

type PacketParticipantsDataParsed struct {
	Header        PacketHeader
	NumActiveCars uint8
	Participants  [22]ParticipantDataParsed
}

type ParticipantDataParsed struct {
	AiControlled  uint8
	DriverId      uint8
	TeamId        uint8
	RaceNumber    uint8
	Nationality   uint8
	Name          string
	YourTelemetry uint8
}
