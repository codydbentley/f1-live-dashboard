package twenty_twenty

const LapDataPacketLength = 1190

// PacketLapData
// The lap data packet gives details of all the cars in the session.
// Frequency: Rate as specified in menus
// Size: 1190 bytes
// Version: 1
// - Header
// - LapData Lap data for all cars on track
type PacketLapData struct {
	Header  PacketHeader
	LapData [22]LapData
}

// LapData
// - LastLapTime                Last lap time in seconds
// - CurrentLapTime             Current time around the lap in seconds
// - Sector1TimeInMS            Sector 1 time in milliseconds
// - Sector2TimeInMS            Sector 2 time in milliseconds
// - BestLapTime                Best lap time of the session in seconds
// - BestLapNum                 Lap number best time achieved on
// - BestLapSector1TimeInMS     Sector 1 time of best lap in the session in milliseconds
// - BestLapSector2TimeInMS     Sector 2 time of best lap in the session in milliseconds
// - BestLapSector3TimeInMS     Sector 3 time of best lap in the session in milliseconds
// - BestOverallSector1TimeInMS Best overall sector 1 time of the session in milliseconds
// - BestOverallSector1LapNum   Lap number best overall sector 1 time achieved on
// - BestOverallSector2TimeInMS Best overall sector 2 time of the session in milliseconds
// - BestOverallSector2LapNum   Lap number best overall sector 2 time achieved on
// - BestOverallSector3TimeInMS Best overall sector 3 time of the session in milliseconds
// - BestOverallSector3LapNum   Lap number best overall sector 3 time achieved on
// - LapDistance                Distance vehicle is around current lap in metres – could be negative if line hasn’t been crossed yet
// - TotalDistance              Total distance travelled in session in metres – could be negative if line hasn’t been crossed yet
// - SafetyCarDelta             Delta in seconds for safety car
// - CarPosition                Car race position
// - CurrentLapNum              Current lap number
// - PitStatus                  0 = none, 1 = pitting, 2 = in pit area
// - Sector                     0 = sector1, 1 = sector2, 2 = sector3
// - CurrentLapInvalid          Current lap invalid - 0 = valid, 1 = invalid
// - Penalties                  Accumulated time penalties in seconds to be added
// - GridPosition               Grid position the vehicle started the race in
// - DriverStatus               Status of driver - 0 = in garage, 1 = flying lap, 2 = in lap, 3 = out lap, 4 = on track
// - ResultStatus               Result status - 0 = invalid, 1 = inactive, 2 = active, 3 = finished, 4 = disqualified, 5 = not classified, 6 = retired
type LapData struct {
	LastLapTime                float32
	CurrentLapTime             float32
	Sector1TimeInMS            uint16
	Sector2TimeInMS            uint16
	BestLapTime                float32
	BestLapNum                 uint8
	BestLapSector1TimeInMS     uint16
	BestLapSector2TimeInMS     uint16
	BestLapSector3TimeInMS     uint16
	BestOverallSector1TimeInMS uint16
	BestOverallSector1LapNum   uint8
	BestOverallSector2TimeInMS uint16
	BestOverallSector2LapNum   uint8
	BestOverallSector3TimeInMS uint16
	BestOverallSector3LapNum   uint8
	LapDistance                float32
	TotalDistance              float32
	SafetyCarDelta             float32
	CarPosition                uint8
	CurrentLapNum              uint8
	PitStatus                  uint8
	Sector                     uint8
	CurrentLapInvalid          uint8
	Penalties                  uint8
	GridPosition               uint8
	DriverStatus               uint8
	ResultStatus               uint8
}
