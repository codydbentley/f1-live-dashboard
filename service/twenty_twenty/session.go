package twenty_twenty

const SessionPacketLength = 251

// PacketSessionData
// The session packet includes details about the current session in progress.
// Frequency: 2 per second
// Size: 251 bytes
// Version: 1
// - Header
// - Weather: 0 = clear, 1 = light cloud, 2 = overcast, 3 = light rain, 4 = heavy rain, 5 = storm
// - TrackTemperature in degrees celsius
// - AirTemperature in degrees celsius
// - TotalLaps in this race
// - TrackLength in metres
// - SessionType: 0 = unknown, 1 = P1, 2 = P2, 3 = P3, 4 = Short P, 5 = Q1, 6 = Q2, 7 = Q3, 8 = Short Q, 9 = OSQ, 10 = R, 11 = R2, 12 = Time Trial
// - TrackId: -1 for unknown, 0-21 for tracks, see appendix
// - Formula: 0 = F1 Modern, 1 = F1 Classic, 2 = F2, 3 = F1 Generic
// - SessionTimeLeft in session in seconds
// - SessionDuration in seconds
// - PitSpeedLimit in kilometres per hour
// - GamePaused Whether the game is paused
// - IsSpectating Whether the player is spectating
// - SpectatorCarIndex Index of the car being spectated
// - SliProNativeSupport: 0 = inactive, 1 = active
// - NumMarshalZones Number of marshal zones to follow
// - MarshalZones List of marshal zones – max 21
// - SafetyCarStatus: 0 = no safety car, 1 = full safety car, 2 = virtual safety car, 3 = formation lap safety car
// - NetworkGame: 0 = offline, 1 = online
// - NumWeatherForecastSamples Number of weather samples to follow
// - WeatherForecastSamples Array of weather forecast samples
type PacketSessionData struct {
	Header                    PacketHeader
	Weather                   uint8
	TrackTemperature          int8
	AirTemperature            int8
	TotalLaps                 uint8
	TrackLength               uint16
	SessionType               uint8
	TrackId                   int8
	Formula                   uint8
	SessionTimeLeft           uint16
	SessionDuration           uint16
	PitSpeedLimit             uint8
	GamePaused                uint8
	IsSpectating              uint8
	SpectatorCarIndex         uint8
	SliProNativeSupport       uint8
	NumMarshalZones           uint8
	MarshalZones              [21]MarshalZone
	SafetyCarStatus           uint8
	NetworkGame               uint8
	NumWeatherForecastSamples uint8
	WeatherForecastSamples    [20]WeatherForecastSample
}

// MarshalZone
// - ZoneStart Fraction (0..1) of way through the lap the marshal zone starts
// - ZoneFlag: -1 = invalid/unknown, 0 = none, 1 = green, 2 = blue, 3 = yellow, 4 = red
type MarshalZone struct {
	ZoneStart float32
	ZoneFlag  int8
}

// WeatherForecastSample
// - SessionType: 0 = unknown, 1 = P1, 2 = P2, 3 = P3, 4 = Short P, 5 = Q1, 6 = Q2, 7 = Q3, 8 = Short Q, 9 = OSQ, 10 = R, 11 = R2, 12 = Time Trial
// - TimeOffset Time in minutes the forecast is for
// - Weather: 0 = clear, 1 = light cloud, 2 = overcast, 3 = light rain, 4 = heavy rain, 5 = storm
// - TrackTemperature Track temp. in degrees celsius
// - AirTemperature Air temp. in degrees celsius
type WeatherForecastSample struct {
	SessionType      uint8
	TimeOffset       uint8
	Weather          uint8
	TrackTemperature int8
	AirTemperature   int8
}
