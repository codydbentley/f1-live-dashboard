package twenty_twenty

const CarSetupsPacketLength = 1102

// PacketCarSetupData
// This packet details the car setups for each vehicle in the session. Note that in multiplayer games, other player
// cars will appear as blank, you will only be able to see your car setup and AI cars.
// Frequency: 2 per second
// Size: 1102 bytes
// Version: 1
// - Header
// - CarSetups
type PacketCarSetupData struct {
	Header    PacketHeader
	CarSetups [22]CarSetupData
}

// CarSetupData
// - FrontWing              Front wing aero
// - RearWing               Rear wing aero
// - OnThrottle             Differential adjustment on throttle (percentage)
// - OffThrottle            Differential adjustment off throttle (percentage)
// - FrontCamber            Front camber angle (suspension geometry)
// - RearCamber             Rear camber angle (suspension geometry)
// - FrontToe               Front toe angle (suspension geometry)
// - RearToe                Rear toe angle (suspension geometry)
// - FrontSuspension        Front suspension
// - RearSuspension         Rear suspension
// - FrontAntiRollBar       Front anti-roll bar
// - RearAntiRollBar        Rear anti-roll bar
// - FrontSuspensionHeight  Front ride height
// - RearSuspensionHeight   Rear ride height
// - BrakePressure          Brake pressure (percentage)
// - BrakeBias              Brake bias (percentage)
// - RearLeftTyrePressure   Rear left tyre pressure (PSI)
// - RearRightTyrePressure  Rear right tyre pressure (PSI)
// - FrontLeftTyrePressure  Front left tyre pressure (PSI)
// - FrontRightTyrePressure Front right tyre pressure (PSI)
// - Ballast                Ballast
// - FuelLoad               Fuel load
type CarSetupData struct {
	FrontWing              uint8
	RearWing               uint8
	OnThrottle             uint8
	OffThrottle            uint8
	FrontCamber            float32
	RearCamber             float32
	FrontToe               float32
	RearToe                float32
	FrontSuspension        uint8
	RearSuspension         uint8
	FrontAntiRollBar       uint8
	RearAntiRollBar        uint8
	FrontSuspensionHeight  uint8
	RearSuspensionHeight   uint8
	BrakePressure          uint8
	BrakeBias              uint8
	RearLeftTyrePressure   float32
	RearRightTyrePressure  float32
	FrontLeftTyrePressure  float32
	FrontRightTyrePressure float32
	Ballast                uint8
	FuelLoad               float32
}
