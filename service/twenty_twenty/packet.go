package twenty_twenty

import "f1-race-control/server-app/twenty_nineteen"

type PacketHeader struct {
	twenty_nineteen.PacketHeader
	SecondaryPlayerCarIndex uint8
}
