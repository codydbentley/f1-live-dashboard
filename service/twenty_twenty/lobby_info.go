package twenty_twenty

const LobbyInfoPacketLength = 1169

// PacketLobbyInfoData
// This packet details the players currently in a multiplayer lobby. It details each player’s selected car, any AI
// involved in the game and also the ready status of each of the participants.
// Frequency: Two every second when in the lobby
// Size: 1169 bytes
// Version: 1
// - Header
// - NumPlayers Number of players in the lobby data
// - LobbyPlayers
type PacketLobbyInfoData struct {
	Header       PacketHeader
	NumPlayers   uint8
	LobbyPlayers [22]LobbyInfoData
}

// LobbyInfoData
// - AiControlled Whether the vehicle is AI (1) or Human (0) controlled
// - TeamId       Team id - see appendix (255 if no team currently selected)
// - Nationality  Nationality of the driver
// - Name         Name of participant in UTF-8 format – null terminated. Will be truncated with ... (U+2026) if too long
// - ReadyStatus  0 = not ready, 1 = ready, 2 = spectating
type LobbyInfoData struct {
	AiControlled uint8
	TeamId       uint8
	Nationality  uint8
	Name         [48]byte
	ReadyStatus  uint8
}

type PacketLobbyInfoDataParsed struct {
	Header       PacketHeader
	NumPlayers   uint8
	LobbyPlayers [22]LobbyInfoDataParsed
}

type LobbyInfoDataParsed struct {
	AiControlled uint8
	TeamId       uint8
	Nationality  uint8
	Name         string
	ReadyStatus  uint8
}
