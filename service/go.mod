module f1-race-control/server-app

go 1.15

require (
	github.com/google/uuid v1.1.2
	github.com/gorilla/websocket v1.4.2
)
