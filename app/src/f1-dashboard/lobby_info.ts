import { PartialPacket } from '@/f1-dashboard/client'

export interface PacketLobbyInfoData extends PartialPacket {
  NumPlayers: number
  LobbyPlayers: LobbyInfoData[]
}

interface LobbyInfoData {
  AiControlled: number
  TeamId: number
  Nationality: number
  Name: string
  ReadyStatus: number
}
