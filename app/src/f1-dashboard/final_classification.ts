import { PartialPacket } from '@/f1-dashboard/client'

export interface PacketFinalClassificationData extends PartialPacket {
  NumCars: number
  ClassificationData: FinalClassificationData[]
}

interface FinalClassificationData {
  Position: number
  NumLaps: number
  GridPosition: number
  Points: number
  NumPitStops: number
  ResultStatus: number
  BestLapTime: number
  TotalRaceTime: number
  PenaltiesTime: number
  NumPenalties: number
  NumTyreStints: number
  TyreStintsActual: number[]
  TyreStintsVisual: number[]
}
