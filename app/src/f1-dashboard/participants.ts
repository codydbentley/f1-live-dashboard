import { PartialPacket } from '@/f1-dashboard/client'

export interface PacketParticipantsData extends PartialPacket {
  NumActiveCars: number
  Participants: ParticipantData[]
}

interface ParticipantData {
  AiControlled: number
  DriverId: number
  TeamId: number
  RaceNumber: number
  Nationality: number
  Name: string
  YourTelemetry: number
}
