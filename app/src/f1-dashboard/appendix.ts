const Wheels = {
  0: 'RL',
  1: 'RR',
  2: 'FL',
  3: 'FR'
}

const Packet = {
  0: 'Motion',
  1: 'Session',
  2: 'Lap Data',
  3: 'Event',
  4: 'Participants',
  5: 'Car Setups',
  6: 'Car Telemetry',
  7: 'Car Status',
  8: 'Final Classification',
  9: 'Lobby information'
}

const Team = {
  0: 'Mercedes',
  1: 'Ferrari',
  2: 'Red Bull Racing',
  3: 'Williams',
  4: 'Racing Point',
  5: 'Renault',
  6: 'Alpha Tauri',
  7: 'Haas',
  8: 'McLaren',
  9: 'Alfa Romeo',
  10: 'McLaren 1988',
  11: 'McLaren 1991',
  12: 'Williams 1992',
  13: 'Ferrari 1995',
  14: 'Williams 1996',
  15: 'McLaren 1998',
  16: 'Ferrari 2002',
  17: 'Ferrari 2004',
  18: 'Renault 2006',
  19: 'Ferrari 2007',
  20: 'McLaren 2008',
  21: 'Red Bull 2010',
  22: 'Ferrari 1976',
  23: 'ART Grand Prix',
  24: 'Campos Vexatec Racing',
  25: 'Carlin',
  26: 'Charouz Racing System',
  27: 'DAMS',
  28: 'Russian Time',
  29: 'MP Motorsport',
  30: 'Pertamina',
  31: 'McLaren 1990',
  32: 'Trident',
  33: 'BWT Arden',
  34: 'McLaren 1976',
  35: 'Lotus 1972',
  36: 'Ferrari 1979',
  37: 'McLaren 1982',
  38: 'Williams 2003',
  39: 'Brawn 2009',
  40: 'Lotus 1978',
  41: 'F1 Generic car',
  42: "Art GP '19",
  43: "Campos '19",
  44: "Carlin '19",
  45: "Sauber Junior Charouz '19",
  46: "Dams '19",
  47: "Uni-Virtuosi '19",
  48: "MP Motorsport '19",
  49: "Prema '19",
  50: "Trident '19",
  51: "Arden '19",
  53: 'Benetton 1994',
  54: 'Benetton 1995',
  55: 'Ferrari 2000',
  56: 'Jordan 1991',
  63: 'Ferrari 1990',
  64: 'McLaren 2010',
  65: 'Ferrari 2010',
  255: 'My Team'
}

const Driver = {
  0: 'Carlos Sainz',
  1: 'Daniil Kvyat',
  2: 'Daniel Ricciardo',
  3: 'Fernando Alonso',
  4: 'Missing4',
  5: 'Missing5',
  6: 'Kimi Raikkonen',
  7: 'Lewis Hamilton',
  8: 'Marcus Ericsson',
  9: 'Max Verstappen',
  10: 'Nico Hulkenberg',
  11: 'Kevin Magnussen',
  12: 'Romain Grosjean',
  13: 'Sebastian Vettel',
  14: 'Sergio Perez',
  15: 'Valtteri Bottas',
  16: 'Missing16',
  17: 'Esteban Ocon',
  18: 'Stoffel Vandoorne',
  19: 'Lance Stroll',
  20: 'Arron Barnes',
  21: 'Martin Giles',
  22: 'Alex Murray',
  23: 'Lucas Roth',
  24: 'Igor Correia',
  25: 'Sophie Levasseur',
  26: 'Jonas Schiffer',
  27: 'Alain Forest',
  28: 'Jay Letourneau',
  29: 'Esto Saari',
  30: 'Yasar Atiyeh',
  31: 'Callisto Calabresi',
  32: 'Naota Izum',
  33: 'Howard Clarke',
  34: 'Wilhelm Kaufmann',
  35: 'Marie Laursen',
  36: 'Flavio Nieves',
  37: 'Peter Belousov',
  38: 'Klimek Michalski',
  39: 'Santiago Moreno',
  40: 'Benjamin Coppens',
  41: 'Noah Visser',
  42: 'Gert Waldmuller',
  43: 'Julian Quesada',
  44: 'Daniel Jones',
  45: 'Artem Markelov',
  46: 'Tadasuke Makino',
  47: 'Sean Gelael',
  48: 'Nyck De Vries',
  49: 'Jack Aitken',
  50: 'George Russell',
  51: 'Maximilian Gunther',
  52: 'Nirei Fukuzumi',
  53: 'Luca Ghiotto',
  54: 'Lando Norris',
  55: 'Sergio Sette Camara',
  56: 'Louis Deletraz',
  57: 'Antonio Fuoco',
  58: 'Charles Leclerc',
  59: 'Pierre Gasly',
  60: 'Brendon Hartler',
  61: 'Sergey Sirotkin',
  62: 'Alexander Albon',
  63: 'Nicholas Latifi',
  64: 'Dorian Boccolacci',
  65: 'Niko Kari',
  66: 'Roberto Merhi',
  67: 'Arjun Maini',
  68: 'Alessio Lorandi',
  69: 'Ruben Meijer',
  70: 'Rashid Nair',
  71: 'Jack Tremblay',
  72: 'Missing72',
  73: 'Missing73',
  74: 'Antonio Giovinazzi',
  75: 'Robert Kubica',
  76: 'Missing76',
  77: 'Missing77',
  78: 'Nobuharu Matsushita',
  79: 'Nikita Mazepin',
  80: 'Guanyu Zhou',
  81: 'Mick Schumacher',
  82: 'Callum Ilott',
  83: 'Juan Manuel Correa',
  84: 'Jordan King',
  85: 'Mahaveer Raghunathan',
  86: 'Tatiana Calderon',
  87: 'Anthoine Hubert',
  88: 'Giuliano Alesi',
  89: 'Ralph Boschung'
}

const Track = {
  0: 'Melbourne',
  1: 'Paul Ricard',
  2: 'Shanghai',
  3: 'Sakhir (Bahrain)',
  4: 'Catalunya',
  5: 'Monaco',
  6: 'Montreal',
  7: 'Silverstone',
  8: 'Hockenheim',
  9: 'Hungaroring',
  10: 'Spa',
  11: 'Monza',
  12: 'Singapore',
  13: 'Suzuka',
  14: 'Abu Dhabi',
  15: 'Texas',
  16: 'Brazil',
  17: 'Austria',
  18: 'Sochi',
  19: 'Mexico',
  20: 'Baku (Azerbaijan)',
  21: 'Sakhir Short',
  22: 'Silverstone Short',
  23: 'Texas Short',
  24: 'Suzuka Short',
  25: 'Hanoi',
  26: 'Zandvoort'
}

const Nationality = {
  0: 'Missing',
  1: 'American',
  2: 'Argentinian',
  3: 'Australian',
  4: 'Austrian',
  5: 'Azerbaijani',
  6: 'Bahraini',
  7: 'Belgian',
  8: 'Bolivian',
  9: 'Brazilian',
  10: 'British',
  11: 'Bulgarian',
  12: 'Cameroonian',
  13: 'Canadian',
  14: 'Chilean',
  15: 'Chinese',
  16: 'Colombian',
  17: 'Costa Rican',
  18: 'Croatian',
  19: 'Cypriot',
  20: 'Czech',
  21: 'Danish',
  22: 'Dutch',
  23: 'Ecuadorian',
  24: 'English',
  25: 'Emirian',
  26: 'Estonian',
  27: 'Finnish',
  28: 'French',
  29: 'German',
  30: 'Ghanaian',
  31: 'Greek',
  32: 'Guatemalan',
  33: 'Honduran',
  34: 'Hong Konger',
  35: 'Hungarian',
  36: 'Icelander',
  37: 'Indian',
  38: 'Indonesian',
  39: 'Irish',
  40: 'Israeli',
  41: 'Italian',
  42: 'Jamaican',
  43: 'Japanese',
  44: 'Jordanian',
  45: 'Kuwaiti',
  46: 'Latvian',
  47: 'Lebanese',
  48: 'Lithuanian',
  49: 'Luxembourger',
  50: 'Malaysian',
  51: 'Maltese',
  52: 'Mexican',
  53: 'Monegasque',
  54: 'New Zealander',
  55: 'Nicaraguan',
  56: 'North Korean',
  57: 'Northern Irish',
  58: 'Norwegian',
  59: 'Omani',
  60: 'Pakistani',
  61: 'Panamanian',
  62: 'Paraguayan',
  63: 'Peruvian',
  64: 'Polish',
  65: 'Portuguese',
  66: 'Qatari',
  67: 'Romanian',
  68: 'Russian',
  69: 'Salvadoran',
  70: 'Saudi',
  71: 'Scottish',
  72: 'Serbian',
  73: 'Singaporean',
  74: 'Slovakian',
  75: 'Slovenian',
  76: 'South Korean',
  77: 'South African',
  78: 'Spanish',
  79: 'Swedish',
  80: 'Swiss',
  81: 'Thai',
  82: 'Turkish',
  83: 'Uruguayan',
  84: 'Ukrainian',
  85: 'Venezuelan',
  86: 'Welsh',
  87: 'Barbadian',
  88: 'Vietnamese'
}

const Surface = {
  0: 'Tarmac',
  1: 'Rumble strip',
  2: 'Concrete',
  3: 'Rock',
  4: 'Gravel',
  5: 'Mud',
  6: 'Sand',
  7: 'Grass',
  8: 'Water',
  9: 'Cobblestone',
  10: 'Metal',
  11: 'Ridged'
}

const Penalty = {
  0: 'Drive through',
  1: 'Stop Go',
  2: 'Grid penalty',
  3: 'Penalty reminder',
  4: 'Time penalty',
  5: 'Warning',
  6: 'Disqualified',
  7: 'Removed from formation lap',
  8: 'Parked too long timer',
  9: 'Tyre regulations',
  10: 'This lap invalidated',
  11: 'This and next lap invalidated',
  12: 'This lap invalidated without reason',
  13: 'This and next lap invalidated without reason',
  14: 'This and previous lap invalidated',
  15: 'This and previous lap invalidated without reason',
  16: 'Retired',
  17: 'Black flag timer'
}

const Infringement = {
  0: 'Blocking by slow driving',
  1: 'Blocking by wrong way driving',
  2: 'Reversing off the start line',
  3: 'Big Collision',
  4: 'Small Collision',
  5: 'Collision failed to hand back position single',
  6: 'Collision failed to hand back position multiple',
  7: 'Corner cutting gained time',
  8: 'Corner cutting overtake single',
  9: 'Corner cutting overtake multiple',
  10: 'Crossed pit exit lane',
  11: 'Ignoring blue flags',
  12: 'Ignoring yellow flags',
  13: 'Ignoring drive through',
  14: 'Too many drive throughs',
  15: 'Drive through reminder serve within n laps',
  16: 'Drive through reminder serve this lap',
  17: 'Pit lane speeding',
  18: 'Parked for too long',
  19: 'Ignoring tyre regulations',
  20: 'Too many penalties',
  21: 'Multiple warnings',
  22: 'Approaching disqualification',
  23: 'Tyre regulations select single',
  24: 'Tyre regulations select multiple',
  25: 'Lap invalidated corner cutting',
  26: 'Lap invalidated running wide',
  27: 'Corner cutting ran wide gained time minor',
  28: 'Corner cutting ran wide gained time significant',
  29: 'Corner cutting ran wide gained time extreme',
  30: 'Lap invalidated wall riding',
  31: 'Lap invalidated flashback used',
  32: 'Lap invalidated reset to track',
  33: 'Blocking the pitlane',
  34: 'Jump start',
  35: 'Safety car to car collision',
  36: 'Safety car illegal overtake',
  37: 'Safety car exceeding allowed pace',
  38: 'Virtual safety car exceeding allowed pace',
  39: 'Formation lap below allowed speed',
  40: 'Retired mechanical failure',
  41: 'Retired terminally damaged',
  42: 'Safety car falling too far back',
  43: 'Black flag timer',
  44: 'Unserved stop go penalty',
  45: 'Unserved drive through penalty',
  46: 'Engine component change',
  47: 'Gearbox change',
  48: 'League grid penalty',
  49: 'Retry penalty',
  50: 'Illegal time gain',
  51: 'Mandatory pitstop'
}

const TractionControl = {
  0: 'Off',
  1: 'Medium',
  2: 'High'
}

const FuelMix = {
  0: 'Lean',
  1: 'Standard',
  2: 'Rich',
  3: 'Max'
}

const ActualTyreCompound = {
  7: 'F1 Intermediate',
  8: 'F1 Wet',
  9: 'Classic Dry',
  10: 'Classic Wet',
  11: 'F2 Super Soft',
  12: 'F2 Soft',
  13: 'F2 Medium',
  14: 'F2 Hard',
  15: 'F2 Wet',
  16: 'F1 C5',
  17: 'F1 C4',
  18: 'F1 C3',
  19: 'F1 C2',
  20: 'F1 C1'
}

const TyreVisualCompound = {
  7: 'F1 Intermediate',
  8: 'F1 Wet',
  9: 'Classic Dry',
  10: 'Classic Wet',
  15: 'F2 Wet',
  16: 'F1 Soft',
  17: 'F1 Medium',
  18: 'F1 Hard',
  19: 'F2 Super Soft',
  20: 'F2 Soft',
  21: 'F2 Medium',
  22: 'F2 Hard'
}

const FiaFlags = {
  0: 'None',
  1: 'Green',
  2: 'Blue',
  3: 'Yellow',
  4: 'Red'
}

const ErsDeployMode = {
  0: 'None',
  1: 'Medium',
  2: 'Overtake',
  3: 'Hotlap'
}

const MfdPanelIndex = {
  0: 'Car Setup',
  1: 'Pits',
  2: 'Damage',
  3: 'Engine',
  4: 'Temperatures',
  255: 'MFD Closed'
}

const ResultStatus = {
  0: 'Invalid',
  1: 'Inactive',
  2: 'Active',
  3: 'Finished',
  4: 'Disqualified',
  5: 'Not Classified',
  6: 'Retired'
}

const PitStatus = {
  0: 'None',
  1: 'Pitting',
  2: 'In Pit'
}

const DriverStatus = {
  0: 'In Garage',
  1: 'Flying Lap',
  2: 'In Lap',
  3: 'Out Lap',
  4: 'On Track'
}

const ReadyStatus = {
  0: 'Not Ready',
  1: 'Ready',
  2: 'Spectating'
}

const Weather = {
  0: 'Clear',
  1: 'Light Clouds',
  2: 'Overcast',
  3: 'Light Rain',
  4: 'Heavy Rain',
  5: 'Storm'
}

const SessionType = {
  0: 'Unknown',
  1: 'Practice 1',
  2: 'Practice 2',
  3: 'Practice 3',
  4: 'Short Practice',
  5: 'Qualifying 1',
  6: 'Qualifying 2',
  7: 'Qualifying 3',
  8: 'Short Qualifying',
  9: 'One Shot Qualifying',
  10: 'Race',
  11: 'Race 2',
  12: 'Time Trial'
}

const Formula = {
  0: 'F1 Modern',
  1: 'F1 Classic',
  2: 'F2',
  3: 'F1 Generic'
}

const SafetyCarStatus = {
  0: 'No Safety Car',
  1: 'Full Safety Car',
  2: 'Virtual Safety Car',
  3: 'Formation Lap Safety Car'
}

type Dictionary = { [key: number]: string }

export class Appendix {
  public Wheels(value: number) {
    return Appendix.getValue(Wheels, value, 'Wheels')
  }
  public Packet(value: number) {
    return Appendix.getValue(Packet, value, 'Packet')
  }
  public Team(value: number) {
    return Appendix.getValue(Team, value, 'Team')
  }
  public Driver(value: number) {
    return Appendix.getValue(Driver, value, 'Driver')
  }
  public Track(value: number) {
    return Appendix.getValue(Track, value, 'Track')
  }
  public Nationality(value: number) {
    return Appendix.getValue(Nationality, value, 'Nationality')
  }
  public Surface(value: number) {
    return Appendix.getValue(Surface, value, 'Surface')
  }
  public Penalty(value: number) {
    return Appendix.getValue(Penalty, value, 'Penalty')
  }
  public Infringement(value: number) {
    return Appendix.getValue(Infringement, value, 'Infringement')
  }
  public TractionControl(value: number) {
    return Appendix.getValue(TractionControl, value, 'TractionControl')
  }
  public FuelMix(value: number) {
    return Appendix.getValue(FuelMix, value, 'FuelMix')
  }
  public ActualTyreCompound(value: number) {
    return Appendix.getValue(ActualTyreCompound, value, 'ActualTyreCompound')
  }
  public TyreVisualCompound(value: number) {
    return Appendix.getValue(TyreVisualCompound, value, 'TyreVisualCompound')
  }
  public FiaFlags(value: number) {
    return Appendix.getValue(FiaFlags, value, 'FiaFlags')
  }
  public ErsDeployMode(value: number) {
    return Appendix.getValue(ErsDeployMode, value, 'ErsDeployMode')
  }
  public MfdPanelIndex(value: number) {
    return Appendix.getValue(MfdPanelIndex, value, 'MfdPanelIndex')
  }
  public ResultStatus(value: number) {
    return Appendix.getValue(ResultStatus, value, 'ResultStatus')
  }
  public PitStatus(value: number) {
    return Appendix.getValue(PitStatus, value, 'PitStatus')
  }
  public DriverStatus(value: number) {
    return Appendix.getValue(DriverStatus, value, 'DriverStatus')
  }
  public ReadyStatus(value: number) {
    return Appendix.getValue(ReadyStatus, value, 'ReadyStatus')
  }
  public Weather(value: number) {
    return Appendix.getValue(Weather, value, 'Weather')
  }
  public SessionType(value: number) {
    return Appendix.getValue(SessionType, value, 'SessionType')
  }
  public Formula(value: number) {
    return Appendix.getValue(Formula, value, 'Formula')
  }
  public SafetyCarStatus(value: number) {
    return Appendix.getValue(SafetyCarStatus, value, 'SafetyCarStatus')
  }
  private static getValue(dict: Dictionary, value: number, name: string): string {
    if (value in dict) {
      return dict[value]
    }
    return `Missing${name}${value}`
  }
}
