import { PartialPacket } from '@/f1-dashboard/client'

export interface PacketCarSetupData extends PartialPacket {
  CarSetups: CarSetupData[]
}

interface CarSetupData {
  FrontWing: number
  RearWing: number
  OnThrottle: number
  OffThrottle: number
  FrontCamber: number
  RearCamber: number
  FrontToe: number
  RearToe: number
  FrontSuspension: number
  RearSuspension: number
  FrontAntiRollBar: number
  RearAntiRollBar: number
  FrontSuspensionHeight: number
  RearSuspensionHeight: number
  BrakePressure: number
  BrakeBias: number
  RearLeftTyrePressure: number
  RearRightTyrePressure: number
  FrontLeftTyrePressure: number
  FrontRightTyrePressure: number
  Ballast: number
  FuelLoad: number
}
