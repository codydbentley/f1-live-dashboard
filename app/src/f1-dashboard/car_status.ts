import { PartialPacket } from '@/f1-dashboard/client'

export interface PacketCarStatusData extends PartialPacket {
  CarStatusData: CarStatusData[]
}

interface CarStatusData {
  TractionControl: number
  AntiLockBrakes: number
  FuelMix: number
  FrontBrakeBias: number
  PitLimiterStatus: number
  FuelInTank: number
  FuelCapacity: number
  FuelRemainingLaps: number
  MaxRPM: number
  IdleRPM: number
  MaxGears: number
  DrsAllowed: number
  DrsActivationDistance: number
  TyresWear: number[]
  ActualTyreCompound: number
  TyreVisualCompound: number
  TyresAgeLaps: number
  TyresDamage: number[]
  FrontLeftWingDamage: number
  FrontRightWingDamage: number
  RearWingDamage: number
  DrsFault: number
  EngineDamage: number
  GearBoxDamage: number
  VehicleFiaFlags: number
  ErsStoreEnergy: number
  ErsDeployMode: number
  ErsHarvestedThisLapMGUK: number
  ErsHarvestedThisLapMGUH: number
  ErsDeployedThisLap: number
}
