/**
 * F1 Live Dashboard Client
 */
import { PacketCarStatusData } from '@/f1-dashboard/car_status'
import { PacketCarSetupData } from '@/f1-dashboard/car_setups'
import { PacketCarTelemetryData } from '@/f1-dashboard/car_telemetry'
import { PacketEventData } from '@/f1-dashboard/events'
import { PacketFinalClassificationData } from '@/f1-dashboard/final_classification'
import { PacketLapData } from '@/f1-dashboard/lap_data'
import { PacketLobbyInfoData } from '@/f1-dashboard/lobby_info'
import { PacketMotionData } from '@/f1-dashboard/motion'
import { PacketSessionData } from '@/f1-dashboard/session'
import { PacketParticipantsData } from '@/f1-dashboard/participants'
import mitt, { Emitter } from 'mitt'
import { Appendix } from '@/f1-dashboard/appendix'

enum PacketId {
  PacketIDMotion,
  PacketIDSession,
  PacketIDLapData,
  PacketIDEvent,
  PacketIDParticipants,
  PacketIDCarSetup,
  PacketIDCarTelemetry,
  PacketIDCarStatus,
  PacketIDFinalClassification,
  PacketIDLobbyInformation
}

export interface PacketHeader {
  PacketFormat: number
  GameMajorVersion: number
  GameMinorVersion: number
  PacketVersion: number
  PacketId: PacketId
  SessionUID: number
  SessionTime: number
  FrameIdentifier: number
  PlayerCarIndex: number
  SecondaryPlayerCarIndex: number
}

export interface PartialPacket {
  Header: PacketHeader
}

class DataSubject<PT extends PartialPacket> {
  private readonly bus: Emitter
  private readonly id: PacketId
  constructor(bus: Emitter, id: PacketId) {
    this.bus = bus
    this.id = id
  }
  public onData(handler: (packet: PT) => void) {
    this.bus.on('data', pkt => {
      if (pkt.Header.PacketId === this.id) {
        handler(pkt as PT)
      }
    })
  }
}

export class F1LiveDashboardClient {
  private socket: WebSocket
  private online: boolean
  private readonly bus: Emitter
  public readonly Appendix: Appendix
  public readonly CarSetup: DataSubject<PacketCarSetupData>
  public readonly CarStatus: DataSubject<PacketCarStatusData>
  public readonly CarTelemetry: DataSubject<PacketCarTelemetryData>
  public readonly Events: DataSubject<PacketEventData>
  public readonly FinalClassification: DataSubject<PacketFinalClassificationData>
  public readonly Lap: DataSubject<PacketLapData>
  public readonly LobbyInfo: DataSubject<PacketLobbyInfoData>
  public readonly Motion: DataSubject<PacketMotionData>
  public readonly Participants: DataSubject<PacketParticipantsData>
  public readonly Session: DataSubject<PacketSessionData>
  constructor() {
    this.online = false
    this.socket = this.connect()
    this.bus = mitt()
    this.Appendix = new Appendix()
    this.CarSetup = new DataSubject<PacketCarSetupData>(this.bus, PacketId.PacketIDCarSetup)
    this.CarStatus = new DataSubject<PacketCarStatusData>(this.bus, PacketId.PacketIDCarStatus)
    this.CarTelemetry = new DataSubject<PacketCarTelemetryData>(this.bus, PacketId.PacketIDCarTelemetry)
    this.Events = new DataSubject<PacketEventData>(this.bus, PacketId.PacketIDEvent)
    this.FinalClassification = new DataSubject<PacketFinalClassificationData>(
      this.bus,
      PacketId.PacketIDFinalClassification
    )
    this.Lap = new DataSubject<PacketLapData>(this.bus, PacketId.PacketIDLapData)
    this.LobbyInfo = new DataSubject<PacketLobbyInfoData>(this.bus, PacketId.PacketIDLobbyInformation)
    this.Motion = new DataSubject<PacketMotionData>(this.bus, PacketId.PacketIDMotion)
    this.Participants = new DataSubject<PacketParticipantsData>(this.bus, PacketId.PacketIDParticipants)
    this.Session = new DataSubject<PacketSessionData>(this.bus, PacketId.PacketIDSession)
  }
  private connect(): WebSocket {
    const socket = new WebSocket('ws://127.0.0.1:20777')
    socket.onopen = () => {
      this.online = true
    }
    socket.onclose = err => {
      this.online = false
      console.log(err)
      setTimeout(() => {
        this.socket = this.connect()
      }, 5000)
    }
    socket.onmessage = msg => {
      const pkt = JSON.parse(msg.data)
      this.bus.emit('data', pkt as PartialPacket)
    }
    return socket
  }
}
