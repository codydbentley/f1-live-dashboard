import { PartialPacket } from '@/f1-dashboard/client'

export interface PacketEventData extends PartialPacket {
  EventStringCode: string
  EventDetails: EventDataDetails
}

type EventDataDetails = FastestLap | Retirement | TeamMateInPits | RaceWinner | Penalty | SpeedTrap | {}

interface FastestLap {
  VehicleIdx: number
  LapTime: number
}

interface Retirement {
  VehicleIdx: number
}

interface TeamMateInPits {
  VehicleIdx: number
}

interface RaceWinner {
  VehicleIdx: number
}

interface Penalty {
  PenaltyType: number
  InfringementType: number
  VehicleIdx: number
  OtherVehicleIdx: number
  Time: number
  LapNum: number
  PlacesGained: number
}

interface SpeedTrap {
  VehicleIdx: number
  Speed: number
}
