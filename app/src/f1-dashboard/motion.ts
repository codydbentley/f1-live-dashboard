import { PartialPacket } from '@/f1-dashboard/client'

export interface PacketMotionData extends PartialPacket {
  CarMotionData: CarMotionData[]
  SuspensionPosition: number[]
  SuspensionVelocity: number[]
  SuspensionAcceleration: number[]
  WheelSpeed: number[]
  WheelSlip: number[]
  LocalVelocityX: number
  LocalVelocityY: number
  LocalVelocityZ: number
  AngularVelocityX: number
  AngularVelocityY: number
  AngularVelocityZ: number
  AngularAccelerationX: number
  AngularAccelerationY: number
  AngularAccelerationZ: number
  FrontWheelsAngle: number
}

interface CarMotionData {
  WorldPositionX: number
  WorldPositionY: number
  WorldPositionZ: number
  WorldVelocityX: number
  WorldVelocityY: number
  WorldVelocityZ: number
  WorldForwardDirX: number
  WorldForwardDirY: number
  WorldForwardDirZ: number
  WorldRightDirX: number
  WorldRightDirY: number
  WorldRightDirZ: number
  GForceLateral: number
  GForceLongitudinal: number
  GForceVertical: number
  Yaw: number
  Pitch: number
  Roll: number
}
