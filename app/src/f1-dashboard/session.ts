import { PartialPacket } from '@/f1-dashboard/client'

export interface PacketSessionData extends PartialPacket {
  Weather: number
  TrackTemperature: number
  AirTemperature: number
  TotalLaps: number
  TrackLength: number
  SessionType: number
  TrackId: number
  Formula: number
  SessionTimeLeft: number
  SessionDuration: number
  PitSpeedLimit: number
  GamePaused: number
  IsSpectating: number
  SpectatorCarIndex: number
  SliProNativeSupport: number
  NumMarshalZones: number
  MarshalZones: MarshalZone[]
  SafetyCarStatus: number
  NetworkGame: number
  NumWeatherForecastSamples: number
  WeatherForecastSamples: WeatherForecastSample[]
}

interface MarshalZone {
  ZoneStart: number
  ZoneFlag: number
}

interface WeatherForecastSample {
  SessionType: number
  TimeOffset: number
  Weather: number
  TrackTemperature: number
  AirTemperature: number
}
