import { PartialPacket } from '@/f1-dashboard/client'

export interface PacketCarTelemetryData extends PartialPacket {
  CarTelemetryData: CarTelemetryData[]
  ButtonStatus: number
  MfdPanelIndex: number
  MfdPanelIndexSecondaryPlayer: number
  SuggestedGear: number
}

interface CarTelemetryData {
  Speed: number
  Throttle: number
  Steer: number
  Brake: number
  Clutch: number
  Gear: number
  EngineRPM: number
  Drs: number
  RevLightsPercent: number
  BrakesTemperature: number[]
  TyresSurfaceTemperature: number[]
  TyresInnerTemperature: number[]
  EngineTemperature: number
  TyresPressure: number[]
  SurfaceType: number[]
}
