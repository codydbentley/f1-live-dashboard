import { PartialPacket } from '@/f1-dashboard/client'

export interface PacketLapData extends PartialPacket {
  LapData: LapData[]
}

interface LapData {
  LastLapTime: number
  CurrentLapTime: number
  Sector1TimeInMS: number
  Sector2TimeInMS: number
  BestLapTime: number
  BestLapNum: number
  BestLapSector1TimeInMS: number
  BestLapSector2TimeInMS: number
  BestLapSector3TimeInMS: number
  BestOverallSector1TimeInMS: number
  BestOverallSector1LapNum: number
  BestOverallSector2TimeInMS: number
  BestOverallSector2LapNum: number
  BestOverallSector3TimeInMS: number
  BestOverallSector3LapNum: number
  LapDistance: number
  TotalDistance: number
  SafetyCarDelta: number
  CarPosition: number
  CurrentLapNum: number
  PitStatus: number
  Sector: number
  CurrentLapInvalid: number
  Penalties: number
  GridPosition: number
  DriverStatus: number
  ResultStatus: number
}
