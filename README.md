# F1 Live Dashboard

This project is for listening to UDP telemetry data from the Codemasters Formula 1 game
series. The intent is to transform the packed binary UDP data into JSON
packets and deliver them to a websocket listening on `localhost`. Doing so
allows for telemetry views to be created using HTML, CSS, and Javascript. 
The 2020 api is fully implemented and working.

### To Run Development:
```
BACKEND:
cd service && go run ./cmd/...

FRONTEND:
cd app && yarn && yarn electron:serve
```

### POIs
Some points of interest in the code:
```
service/cmd/main.go
service/f1_api/api.go
service/twenty_twenty/decoders.go

app/src/f1-dashboard/client.ts
app/src/f1-dashboard/events.ts
```
